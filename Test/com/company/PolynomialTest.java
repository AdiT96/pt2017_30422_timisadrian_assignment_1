package com.company;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Adi on 13-Mar-17.
 */
public class PolynomialTest {



    @Test
    public void addPolynomials() throws Exception {
        Polynomial p = new Polynomial();
        Polynomial q = new Polynomial();
        Polynomial result = new Polynomial();
        p.readPolynomial("+12x^4+2.345x^3-4x^1+5.78x^0");
        q.readPolynomial("-4x^5+34x^3-3.432x^1");
        result = Polynomial.addPolynomials(p, q);

    }

    @Test
    public void subtractPolynomials() throws Exception {
        Polynomial p = new Polynomial();
        Polynomial q = new Polynomial();
        Polynomial result = new Polynomial();
        p.readPolynomial("+12x^4+2.345x^3-4x^1+5.78x^0");
        q.readPolynomial("-4x^5+34x^3-3.432x^1");
        result = Polynomial.subtractPolynomials(p,q);

    }

    @Test
    public void derivatePolynomial() throws Exception {
        Polynomial p = new Polynomial();
        p.readPolynomial("+12x^4+2.345x^3-4x^1+5.78x^0");
        p.derivatePolynomial();
    }

    @Test
    public void integratePolynomial() throws Exception {
        Polynomial p = new Polynomial();
        p.readPolynomial("+12x^4+2.345x^3-4x^1+5.78x^0");
        p.integratePolynomial();
    }

    @Test
    public void multiplyPolynomials() throws Exception {
        Polynomial p = new Polynomial();
        Polynomial q = new Polynomial();
        p.readPolynomial("+12x^4+2.345x^3-4x^1+5.78x^0");
        q.readPolynomial("3x^2+2x^0");
        Polynomial result = new Polynomial();
        result = Polynomial.multiplyPolynomials(p, q);

    }

    @Test
    public void dividePolynomials() throws Exception {
        Polynomial p = new Polynomial();
        p.readPolynomial("12x^4-6x^3+2x^1+7x^0");
        Polynomial q = new Polynomial();
        q.readPolynomial("3x^2+2x^0");
        List<Polynomial> results = new ArrayList<Polynomial>();
        Polynomial r1 = new Polynomial();
        Polynomial r2 = new Polynomial();
        results.add(r1);
        results.add(r2);
        results = Polynomial.dividePolynomials(p,q);

    }

}