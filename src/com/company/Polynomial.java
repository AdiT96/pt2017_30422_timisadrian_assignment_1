package com.company;
import java.util.*;

/**
 * Created by Adi on 05-Mar-17.
 *
 *  This class consists of methods for creating polynomials, and doing operations on them.
 *  The operations that work on 2 polynomials are declared as public static,
 *  while the operations that work on only one polynomial are not declared as static.
 *
 */
public class Polynomial {


    private List<DoubleMonom> dmon;
    private Integer degree;

    public Polynomial(){

        this.dmon = new ArrayList<DoubleMonom>();
        this.degree = 0;
    }

    public void setDegree(Integer d){this.degree = d;}

    public List<DoubleMonom> getDmon() {
        return dmon;
    }

    public Integer getDegree() {
        return degree;
    }


    /*
    *
    * This method was created to eliminate monomials which have zeroes as coefficients.
    * The method is usually called after doing some operations on a polynomial.
    * It works by creating a copy list of DoubleMonom of the polynomial from which we intend
    * to eliminate the zeroes, and as it finds a monomial which it wants to eliminate,
    * it removes it from the given polynomial.
    *
     */
    private void eliminateZeroes(){
        List<DoubleMonom> aux = new ArrayList<DoubleMonom>();
        aux.addAll(this.getDmon());
        for (DoubleMonom d: aux) {
            if (d.getCoefficient() == 0) this.getDmon().remove(d);
        }
    }


    /*
    *
    * This method does an addition operation between two polynomials and returns a result polynomial.
    * It is declared as public static so that it can be called without instantiating an object of type
    * Polynomial.
    *
     */
    public static Polynomial addPolynomials(Polynomial p1, Polynomial p2){
        Polynomial result  = new Polynomial();
        boolean found;
        for (DoubleMonom d1 : p1.getDmon()){
            found = false;
            for (DoubleMonom d2 : p2.getDmon()){
                if (d1.getExponent() == d2.getExponent()){
                    result.addDoubleMonom(new DoubleMonom(d1.getCoefficient() + d2.getCoefficient(), d1.getExponent()));
                    // Because there is a monomial with the same exponent in both the p and q polynomials, you add a new
                    // one in the result polynomial.
                    found = true; // Set the boolean variable as true, so that we know whether to add the monomial
                                  // from the p polynomial to the result polynomial as it is
                }
            }
            if (!found) result.addDoubleMonom(d1); // There is no monomial in the q polynomial with the same exponent
                                                   // as the one in the p polynomial, so that monomial is added as it is
                                                   // in the result polynomial.
        }
        // We now check to see if some monomials from the q polynomial which might have an exponent different from
        // any exponent from monomials in the p polynomial, so that we add that monomial to the result polynomial
        for (DoubleMonom d1 : p2.getDmon()){
            found = false;
            for (DoubleMonom d2 : result.getDmon()){
                if (d1.getExponent() == d2.getExponent())
                    found = true; // This means that the monomial in the q polynomial has already been added in the
                                  // previous stage
            }
            if (!found) result.addDoubleMonom(d1);
        }
        result.eliminateZeroes(); // Eliminate any DoubleMonoms with zero coefficients which might have resulted from the operation
        Collections.sort(result.getDmon(), new DoubleComparator()); // Sort the resulting array so that the highest monomial is the first
        result.setDegree(result.getDmon().get(0).getExponent()); // Update the degree of the resulting polynomial
        return result;
    }


    /*
    *
    * This method does a subtraction between two polynomials and returns a result polynomial.
    * Because it is very similar to the addition operation, only where there is a difference
    * will there be a comment, if further clarification is needed, please check the comments
    * from the addition operation.
    *
     */
    public static Polynomial subtractPolynomials(Polynomial p1, Polynomial p2){
        Polynomial result  = new Polynomial();
        boolean found;
        for (DoubleMonom d1 : p1.getDmon()){
            found = false;
            for (DoubleMonom d2 : p2.getDmon()){
                if (d1.getExponent() == d2.getExponent()){

                    result.addDoubleMonom(new DoubleMonom(d1.getCoefficient() - d2.getCoefficient(), d1.getExponent())); // We add the difference between the coefficients

                    found = true;
                }
            }
            if (!found) result.addDoubleMonom(d1);
        }

        for (DoubleMonom d1 : p2.getDmon()){
            found = false;
            for (DoubleMonom d2 : result.getDmon()){
                if (d1.getExponent() == d2.getExponent())
                    found = true;
            }
            if (!found) {
                result.addDoubleMonom(new DoubleMonom((-1)*d1.getCoefficient(), d1.getExponent())); //  We add the inverse of the coefficient
            }
        }
        result.eliminateZeroes();
        Collections.sort(result.getDmon(), new DoubleComparator());
        result.setDegree(result.getDmon().get(0).getExponent());
        return result;
    }

    /*
    *
    * This method differentiates a polynomial. It is not declared as static because we want to change
    * the polynomial, and not store the result in another polynomial.
    *
     */
    public void derivatePolynomial(){
        List<DoubleMonom> aux = new ArrayList<DoubleMonom>(); // Create an empty list
        aux.addAll(this.dmon); // Copy the monomials of the polynomial which we want to differentiate to the list
        for (DoubleMonom d : aux){
            if (d.getExponent() == 0) dmon.remove(d); // If the exponent of a monomial is 0, remove it from the polynomial
            else d.derivateMonom(); // Otherwise, call the differentiate method from the DoubleMonom class
        }
        if (!this.getDmon().isEmpty()) // Update the degree of the polynomial
        this.degree = this.dmon.get(0).getExponent();
        else{
            this.degree = 0;
        }
    }

    /*
    *
    * This method was created to integrate a polynomial. It is not declared as static because we want to change
    * the polynomial.
    *
     */
    public void integratePolynomial(){
        for (DoubleMonom d : dmon)
            d.integrateMonom(); // For each monomial in the polynomial, call the integrate method in the DoubleMonom class
        this.degree = this.dmon.get(0).getExponent(); // Update the degree
    }

    /*
    *
    * This method returns a DoubleMonom from a given list if there is a monomial with the exponent e given
    * as a parameter. It is used in the multipltPolynomials method
    *
     */
    private static DoubleMonom findByExponent(List<DoubleMonom> dm , int e){
        for (DoubleMonom d : dm)
            if (d.getExponent() == e) return d; // Performs a simple search through the list
        return null;
    }

    /*
    *
    * This method was created to perform the multiply operation between two polynomials.
    * It is declared as static because we do not want to change the polynomials, and therefore
    * there is no need to call this method by instantiating an object of type Polynomial.
    *
     */
    public static Polynomial multiplyPolynomials(Polynomial p1 , Polynomial p2){
        Polynomial result = new Polynomial(); // Initialize an empty result polynomial
        for(DoubleMonom d1 : p1.getDmon()){ // This double for loop basically multiplies each monomial of the first polynomial
            for (DoubleMonom d2 : p2.getDmon()) // with each monomial of the second polynomial
            {
                if (findByExponent(result.getDmon(), d1.getExponent() + d2.getExponent()) != null) {
                    // If this if is true, it means that there is already a monomial with the same exponent in the
                    // result polynomial, therefore, we remove it from the result polynomial, and add a new one
                    // which is the sum of the former one and the new one.
                    DoubleMonom daux = findByExponent(result.getDmon(), d1.getExponent() + d2.getExponent());
                    result.getDmon().remove(daux);
                    result.addDoubleMonom(new DoubleMonom(daux.getCoefficient() + d1.getCoefficient()*d2.getCoefficient(), daux.getExponent()));
                }
                else result.addDoubleMonom(new DoubleMonom(d1.getCoefficient() * d2.getCoefficient() , d1.getExponent() + d2.getExponent()));
                // Otherwise, just add the new monomial to the result polynomial
            }
        }
        Collections.sort(result.getDmon() , new DoubleComparator()); // Sort the result polynomial
        result.setDegree(result.getDmon().get(0).getExponent()); // Update the degree
        return result;
    }


    /*
    *
    * This method was created to perform the division operation between two polynomials.
    * It is declared as static because we do no want to change the two polynomials given as arguments,
    * and we therefore don't need to instantiate an object of type Polynomial.
    *
     */
    public static List<Polynomial> dividePolynomials(Polynomial p, Polynomial q){
        List<Polynomial> result = new ArrayList<Polynomial>(); // Create an empty result List<Polynomial>
        Polynomial cat = new Polynomial(); // Create an empty cat polynomial
        while(p.getDegree() > q.getDegree()){
            DoubleMonom daux;
            if (Math.abs(p.getDmon().get(0).getCoefficient()) < Math.abs(q.getDmon().get(0).getCoefficient()))
                daux = new DoubleMonom(p.getDmon().get(0).getCoefficient() / q.getDmon().get(0).getCoefficient(), p.getDmon().get(0).getExponent() - q.getDmon().get(0).getExponent());
            else
                daux = new DoubleMonom((p.getDmon().get(0).getCoefficient().intValue() / q.getDmon().get(0).getCoefficient().intValue()) * 1.0, p.getDmon().get(0).getExponent() - q.getDmon().get(0).getExponent());
            // Create the DoubleMonom to be added to cat
            cat.addDoubleMonom(daux); // Add this to cat
            Polynomial aux = new Polynomial(); // Create new polynomial
            aux.addDoubleMonom(daux); // Add daux to the newly created polynomial
            aux = Polynomial.multiplyPolynomials(aux, q); // Multiply aux with q
            p = Polynomial.subtractPolynomials(p, aux); // Subtract aux from p
        }
        if (p.getDegree() == q.getDegree() && Math.abs(p.getDmon().get(0).getCoefficient()) >= Math.abs(q.getDmon().get(0).getCoefficient())){
            // If the condition is true, do the step in the while loop once more
            DoubleMonom daux2 = new DoubleMonom((p.getDmon().get(0).getCoefficient().intValue() / q.getDmon().get(0).getCoefficient().intValue()) * 1.0, p.getDmon().get(0).getExponent() - q.getDmon().get(0).getExponent());
            cat.addDoubleMonom(daux2);
            Polynomial aux = new Polynomial();
            aux.addDoubleMonom(daux2);
            aux = Polynomial.multiplyPolynomials(aux, q);
            p = Polynomial.subtractPolynomials(p, aux);
        }
        result.add(cat); // Add cat and p (which is now the remainder) to the List
        result.add(p);
        return result;
    }




    public void addDoubleMonom(DoubleMonom d){
        dmon.add(d);
    }


    /*
    *
    * This method was created to print a polynomial.
    * It returns a string of all the monomials that form the polynomial.
    * It takes advantage of the toString method from DoubleMonom
    *
     */
    public String printPolynomial(){
        String s = "";
        for (DoubleMonom d : dmon){
            s = s + d;
        }
        return s;
    }


    /*
    *
    * This method was created to check if a string can be parsed to a double.
    * Although there might not necessarily be a need for it, it was kept
    * from an older implementation of the problem, which wanted to parse
    * the integers as integers, and not as double
    *
     */
    private static char readCheck(String c){
        boolean d = false;
        for (int i = 0; i < c.length(); i++)
            if (c.charAt(i) == '.')
                d = true;
            else if (c.charAt(i) < '0' || c.charAt(i) > '9') return 'f';
        return 'd';
    }


    /*
    *
    * This method was created to read s polynomial from a given String.
     * It uses the very powerful split method, to split the String by x, and signs,
     * but it actually keeps the signs in the split Strings as well.
     * It also throws a NumberFormatException which is dealt with in the model.
    *
     */
    public void readPolynomial(String pol) throws NumberFormatException{
        String[] split = pol.split("[x]|(?=[+])|(?=[-])");
        String aux;
        Integer exp;
        Double coeff;
        if (split.length % 2 != 0) throw new NumberFormatException();
        else {
            for (int i = 0; i < split.length; i += 2) {
                if (readCheck(split[i].substring(1)) == 'd') {
                    coeff = Double.parseDouble(split[i]);
                } else throw new NumberFormatException();
                if (split[i + 1].charAt(0) == '^') { // Vhecks to see if the string is an exponent
                    aux = split[i + 1].substring(1);
                    exp = Integer.parseInt(aux);
                } else throw new NumberFormatException();
                DoubleMonom dm = new DoubleMonom(coeff, exp);
                dmon.add(dm);
            }
        }
        Collections.sort(this.dmon, new DoubleComparator()); // Sort the polynomial after the reading
        this.degree = this.dmon.get(0).getExponent(); // Update the degree
    }
}
