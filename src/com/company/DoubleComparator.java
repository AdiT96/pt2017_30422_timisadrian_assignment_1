package com.company;
import java.util.*;

/**
 * Created by Adi on 06-Mar-17.
 */
public class DoubleComparator implements Comparator<DoubleMonom> {

    @Override
    public int compare(DoubleMonom d1, DoubleMonom d2){
        return (-1)*Integer.compare(d1.getExponent(), d2.getExponent());
    }
}
