package com.company;

import java.util.*;

/**
 * Created by Adi on 05-Mar-17.
 *
 *  This Class is the basic building block of the polynomial class, as a polynomial is composed of
 *  a series of monomials. This DoubleMonom class, has as the name suggests, a Double for the coefficient.
 */


public class DoubleMonom{

    private Double coefficient;
    private Integer exponent;



    public DoubleMonom(){
        this.coefficient = 0.0;
        this.exponent = 0;
    }

    public Integer getExponent() {
        return exponent;
    }

    public DoubleMonom(Double c, Integer e){
        this.coefficient = c;
        this.exponent = e;

    }

    public Double getCoefficient() {
        return coefficient;
    }

    /*
    *
    *  This toString method overrides the one in the Object class.
    *  It also prints the numbers in such a way that integers, even though they are saved
    *  on a Double variable, are still printed as though they are Integers.
    *
     */
    public String toString(){
        if (this.coefficient == Math.round(this.coefficient))
            return String.format("%+dx^%d", this.coefficient.intValue(), this.exponent);
        else
            return String.format("%+.3fx^%d", this.coefficient, this.exponent);
    }


    /*
    *
    * This method differentiates a single DoubleMonom
    *
     */
    public void derivateMonom(){
        this.coefficient = this.coefficient * this.exponent;
        this.exponent = this.exponent - 1;
    }

    /*
    *
    * This method integrates a single DoubleMonom
    *
     */
    public void integrateMonom(){
        this.coefficient = this.coefficient / (this.exponent + 1);
        this.exponent++;
    }

}
