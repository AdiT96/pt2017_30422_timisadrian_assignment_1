package com.company;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adi on 10-Mar-17.
 */
public class Model {



    public Model(){
       }


    public String addPolynomials(String p1, String p2){
        Polynomial p = new Polynomial();
        Polynomial q = new Polynomial();
        try{
            p.readPolynomial(p1);
            q.readPolynomial(p2);}
        catch (NumberFormatException n){
            return "The polynomial was badly introduced";
        }
        Polynomial r = Polynomial.addPolynomials(p,q);
        return r.printPolynomial();

    }


    public String subtractPolynomials(String p1, String p2){
        Polynomial p = new Polynomial();
        Polynomial q = new Polynomial();
        try {
            p.readPolynomial(p1);
            q.readPolynomial(p2);
        }catch(NumberFormatException n){
            return "The polynomial was badly introduced";
        }
        Polynomial r = Polynomial.subtractPolynomials(p,q);
        return r.printPolynomial();
    }

    public String multiplyPolynomials(String p1, String p2){
        Polynomial p = new Polynomial();
        Polynomial q = new Polynomial();
        try {
            p.readPolynomial(p1);
            q.readPolynomial(p2);
        }catch(NumberFormatException n){
            return "The polynomial was badly introduced";
        }
        Polynomial r = Polynomial.multiplyPolynomials(p, q);
        return r.printPolynomial();
    }

    public String differentiate(String p1){
        Polynomial p = new Polynomial();
        try{
            p.readPolynomial(p1);
        }catch (NumberFormatException n){
            return "The polynomial was badly introduced";
        }
        p.derivatePolynomial();
        return p.printPolynomial();
    }

    public String integrate(String p1){
        Polynomial p = new Polynomial();
        try{
            p.readPolynomial(p1);
        }
        catch (NumberFormatException n){
            return "The polynomial was badly introduced";
        }
        p.integratePolynomial();
        return p.printPolynomial();
    }

    public String[] divide(String p1, String p2){
        List<Polynomial> p = new ArrayList<Polynomial>();
        Polynomial po = new Polynomial();
        Polynomial q = new Polynomial();
        p.add(po);
        p.add(q);
        String[] returned =  new String[2];

        try{
            p.get(0).readPolynomial(p1);
            p.get(1).readPolynomial(p2);
        }catch(NumberFormatException n){
            returned[0] = "The polynomial was badly introduced";
            returned[1] = "The polynomial was badly introduced";
            return returned;
        }
        p = Polynomial.dividePolynomials(p.get(0), p.get(1));
        returned[0] = p.get(0).printPolynomial();
        returned[1] = p.get(1).printPolynomial();
        return returned;


    }

}
