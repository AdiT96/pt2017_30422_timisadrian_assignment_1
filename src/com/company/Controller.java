package com.company;

import javax.swing.*;
import java.awt.event.*;

/**
 * Created by Adi on 10-Mar-17.
 */
public class Controller {

    private Model m;
    private View v;


    public Controller(Model mod, View vi){
        m = mod;
        v = vi;

        v.addAddListener(new AddListener());
        v.addSubtractListener(new SubtractListener());
        v.addMultiplyListener(new MultiplyListener());
        v.addDifferentiateFirstListener(new DifferentiateFirstListener());
        v.addIntegrateFirstListener(new IntegrateFirstListener());
        v.addDifferentiateSecondListener(new DifferentiateSecondListener());
        v.addIntegrateSecondListener(new IntegrateSecondListener());
        v.addDivideListener(new DivideListener());

    }


    class DivideListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            v.setResult(m.divide(v.getFirstPolynomial(), v.getSecondPolynomial())[0]);
            v.setRemainder(m.divide(v.getFirstPolynomial(), v.getSecondPolynomial())[1]);
        }
    }


    class AddListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            v.setResult(m.addPolynomials(v.getFirstPolynomial(), v.getSecondPolynomial()));
            v.setRemainder("");
        }
    }

    class SubtractListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            v.setResult(m.subtractPolynomials(v.getFirstPolynomial(), v.getSecondPolynomial()));
            v.setRemainder("");
        }
    }

    class MultiplyListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            v.setResult(m.multiplyPolynomials(v.getFirstPolynomial(), v.getSecondPolynomial()));
            v.setRemainder("");
        }
    }

    class DifferentiateFirstListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            v.setFirstPolynomial(m.differentiate(v.getFirstPolynomial()));
        }
    }

    class IntegrateFirstListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            v.setFirstPolynomial(m.integrate(v.getFirstPolynomial()));
        }
    }

    class DifferentiateSecondListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            v.setSecondPolynomial(m.differentiate(v.getSecondPolynomial()));
        }
    }

    class IntegrateSecondListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            v.setSecondPolynomial(m.integrate(v.getSecondPolynomial()));
        }
    }
}
